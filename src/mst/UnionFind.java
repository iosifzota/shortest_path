package mst;

/**
 * Complexity: O(lg* N)
 */
public class UnionFind {
    private int[] id;
    private int[] componentSize;

    public UnionFind(int size) {
        id = new int[size];
        componentSize = new int[size];
        // initialize state
        for (int i = 0; i < size; ++i) {
            id[i] = i;
            componentSize[i] = 1;
        }
    }

    public int root(int elemId) {
        // wall up the 'tree'
        while(elemId != id[elemId]) {
            id[elemId] = id[id[elemId]]; // path compression
            elemId = id[elemId];
        }
        return elemId;
    }

    public boolean connected(int id, int otherId) {
        id = root(id);
        otherId = root(otherId);
        return id == otherId;
    }

    public void union(int id, int toId) {
        id = root(id);
        toId = root(toId);

        // smallest tree binds to the bigger one (weighted union)
        if (componentSize[id] > componentSize[toId]) {
            this.id[toId] = id;
        } else {
            this.id[id] = toId;
        }
    }
}
