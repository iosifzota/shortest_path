package mst;

import containers.Int;

public class Test {
    private Test() {}

    public static void testGraph() {
        Graph<String, Integer, Integer> g;

        g = new Graph<>();
        if (!g.isEmpty()) {
            System.out.println("18");
            throw new AssertionError();
        }

        g.addVertex("kabal", 23);
        if (!g.hasVertex("kabal")) {
            System.out.println("24");
            throw new AssertionError();
        }

        g.addVertex("trash", 63);
        if (!g.hasVertex("trash")) {
            System.out.println("30");
            throw new AssertionError();
        }

        g.insertEdgeBi("kabal", "trash", 9);
        if (!g.hasEdgeOneWay("kabal", "trash")) {
            System.out.println("36");
            throw new AssertionError();
        }

        g.insertEdgeBi("kabal", "tumbleweed", 10);
        if (g.hasEdgeOneWay("kabal", "tumbleweed")) {
            System.out.println("42");
            throw new AssertionError();
        }

        g.removeEdgeOneWay("kabal", "trash");
        if (g.hasEdgeOneWay("kabal", "trash")) {
            System.out.println("48");
            throw new AssertionError();
        }
        if (!g.hasEdgeOneWay("trash", "kabal")) {
            System.out.println("49");
            throw new AssertionError();
        }

        g.removeNode("trash");
        if (g.hasVertex("trash")) {
            System.out.println("58");
            throw new AssertionError();
        }
        if (g.hasEdgeOneWay("trash", "kabal")) {
            System.out.println("62");
            throw new AssertionError();
        }

        g.addVertex("desert", 1);
        if (!g.hasVertex("desert")) {
            System.out.println("68");
            throw new AssertionError();
        }

        g.insertEdgeBi("desert", "kabal", 29);
        if (!g.hasEdgeOneWay("desert", "kabal")) {
            System.out.println("74");
            throw new AssertionError();
        }

        g.removeEdgeBi("desert", "kabal");
        if (g.hasEdgeOneWay("desert", "kabal")) {
            System.out.println("80");
            throw new AssertionError();
        }

        g.insertEdgeBi("kabal", "desert", 21);
        if (!g.hasEdgeOneWay("kabal", "desert") || !g.hasEdgeOneWay("desert", "kabal"))  {
            System.out.println(86);
            throw new AssertionError();
        }
        if (!g.hasEdge("kabal", "desert")) {
            System.out.println(91);
            throw new AssertionError();
        }

        if (g.isEmpty()) {
            System.out.println("92");
            throw new AssertionError();
        }

        g.addVertex("canada", 2);
        if (!g.hasVertex("canada")) {
            System.out.println("98");
            throw new AssertionError();
        }

        g.insertEdgeBi("canada", "desert", 21);
        if (!g.hasEdgeOneWay("canada", "desert") || !g.hasEdgeOneWay("desert", "canada"))  {
            System.out.println(104);
            throw new AssertionError();
        }
        if (!g.hasEdge("canada", "desert")) {
            System.out.println(91);
            throw new AssertionError();
        }

        for (String neighbor : g.getNeighbors("desert")) {
            System.out.println(neighbor);
        }

        System.out.println("test_graph(): DONE\n");
    }

    public static void other() {
        Graph<Int, String, Integer> graph = new Graph<>();

        graph.addVertex(Int.mk(4), "four");
        graph.addVertex(Int.mk(3), "three");

        String s = graph.getNodeData(Int.mk(3));
        System.out.println(s);

        graph.insertEdgeBi(Int.mk(4), Int.mk(3), 7);
        assertThis(graph.hasEdgeOneWay(Int.mk(4), Int.mk(3)) && graph.hasEdgeOneWay(Int.mk(3), Int.mk(4)), "Should have edge");

    }

    public static void testUnionFind() {
        UnionFind uf = new UnionFind(10);
        uf.union(4, 3);
        assertThis(uf.connected(4, 3), "Must be connected");
        uf.union(3, 8);
        assertThis(uf.connected(3, 8), "Must be connected");
        assertThis(uf.connected(4, 8), "Must be connected");
        uf.union(6,5);
        assertThis(uf.connected(6, 5), "Must be connected");
        uf.union(9, 4);
        assertThis(uf.connected(9, 4), "Must be connected");
        assertThis(uf.connected(3, 9), "Must be connected");
        assertThis(uf.connected(8, 9), "Must be connected");
        uf.union(2, 1);
        assertThis(uf.connected(2, 1), "Must be connected");

        //
        assertThis(!uf.connected(0, 7), "Musn't be connected");

        uf.union(5, 0);
        assertThis(uf.connected(5, 0), "Must be connected");
        assertThis(uf.connected(6, 0), "Must be connected");
        uf.union(7, 2);
        assertThis(uf.connected(7, 2), "Must be connected");
        assertThis(uf.connected(7, 1), "Must be connected");
        uf.union(6, 1);
        assertThis(uf.connected(6, 1), "Must be connected");
        assertThis(uf.connected(6, 2), "Must be connected");
        assertThis(uf.connected(6, 7), "Must be connected");
        assertThis(uf.connected(5, 1), "Must be connected");
        assertThis(uf.connected(5, 2), "Must be connected");
        assertThis(uf.connected(5, 7), "Must be connected");
        assertThis(uf.connected(0, 1), "Must be connected");
        assertThis(uf.connected(0, 2), "Must be connected");
        assertThis(uf.connected(0, 7), "Must be connected now");
        uf.union(1, 0);
        assertThis(uf.connected(0, 1), "Must be connected same again");

    }

    public static void assertThis(Boolean b, String errMsg) throws AssertionError {
        if (!b) {
            throw new AssertionError(errMsg);
        }
    }
}
