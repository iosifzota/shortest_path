package mst;

import containers.Pair;
import containers.Triplet;
import functional.TriConsumer;

import java.util.*;
import java.util.function.*;

/**
 * Digraph (directed & undired)
 *
 * Done: node() -> optional
 * TODO: edge() -> optional
 * Done: getNeighbors() -> optional
 * Done: edgesOfNode() -> optional
 * EDIT: Removed implenents Comparable<>; meh
 * @param <NodeKey>
 * @param <NodeData>
 * @param <EdgeData>
 */
public class Graph<NodeKey, NodeData, EdgeData extends Comparable<EdgeData> > {
    final private HashMap<NodeKey, NodeData> nodes;
    final private HashMap<NodeKey, HashMap<NodeKey, EdgeData>> edges;  // key -> (key -> edge)

    public final int cap;

    public Graph() {
        nodes = new HashMap<>();
        edges = new HashMap<>();
        this.cap = -1;
    }

    public Graph(int capacity) {
        this.cap = capacity;
        nodes = new HashMap<>(capacity);
        edges = new HashMap<>(capacity);
    }

    public int getCap() {
        return cap;
    }

    // No edgesCardinal because it's way to expensive.
    public int getNumVertices() {
        return nodes.size();
    }

    public boolean isEmpty() {
        return nodes.isEmpty();
    }

    public boolean hasVertex(NodeKey k) {
        if (null == k) return false;
        return nodes.containsKey(k);
    }

    public Set<NodeKey> getNodeKeys() {
        return nodes.keySet();
    }

    public void addVertex(NodeKey k, NodeData d) {
        nodes.putIfAbsent(k, d);
    }

    public NodeData getNodeData(NodeKey k) {
        return nodes.get(k);
    }

    /**
     * Remove all edges to node.
     * @param k
     */
    public void cutoffNode(NodeKey k) {
        HashMap<NodeKey, EdgeData> endToEdge = edges.get(k);

        if (endToEdge == null) {
            return;
        }

        // rm edges ... -> k
        for (NodeKey currentEndKey : endToEdge.keySet()) {
            removeEdgeOneWay(currentEndKey, k);
        }

        // rm edges k -> ...
        edges.remove(k);
    }

    /**
     * cutoffNode(k) and remove the node.
     * @param k
     */
    public void removeNode(NodeKey k) {
        nodes.remove(k);
        cutoffNode(k);
    }

    public boolean hasEdgeOneWay(NodeKey begin, NodeKey end) {
        HashMap<NodeKey,EdgeData> endToEdge = edges.get(begin);
        // mk sure not to dref null
        return (endToEdge != null) && endToEdge.containsKey(end);
    }

    public EdgeData getEdgeData(NodeKey begin, NodeKey end) {
        HashMap<NodeKey,EdgeData> endToEdge = edges.get(begin);
        // mk sure not to dref null
        if (endToEdge != null) {
            return endToEdge.get(end);
        }
        return null;
    }

    /**
     * Add edge one way.
     * @param begin
     * @param end
     * @param edgeData
     */
    private void insertEdgeOneWay(NodeKey begin, NodeKey end, EdgeData edgeData) {
        // insert edge iff *begin*, *end* are in the *nodes* set.
        if (!hasVertex(begin) || !hasVertex(end))
            return;

        edges.putIfAbsent(begin, new HashMap<>());
        edges.get(begin).putIfAbsent(end, edgeData); // over rights
    }

    /**
     * (Over writes old edge) Add edge one way.
     * @param begin
     * @param end
     * @param edgeData
     */
    private void putEdgeOneWay(NodeKey begin, NodeKey end, EdgeData edgeData) {
        // insert edge iff *begin*, *end* are in the *nodes* set.
        if (!hasVertex(begin) || !hasVertex(end))
            throw new IllegalArgumentException("Invalid vertices");

        edges.putIfAbsent(begin, new HashMap<>());
        edges.get(begin).put(end, edgeData); // over rights
    }

    /**
     * Add (begin, end) & (end, begin).
     * @param begin
     * @param end
     * @param edgeData
     */
    public void insertEdgeBi(NodeKey begin, NodeKey end, EdgeData edgeData) {
        insertEdgeOneWay(begin, end, edgeData);
        insertEdgeOneWay(end, begin, edgeData);
    }

    /**
     * (Over writes old edges) Add (begin, end) & (end, begin).
     * @param begin
     * @param end
     * @param edgeData
     */
    public void addEdge(NodeKey begin, NodeKey end, EdgeData edgeData) {
        putEdgeOneWay(begin, end, edgeData);
        putEdgeOneWay(end, begin, edgeData);
    }

    public Set<NodeKey> getEdgeBeginnings() {
        return edges.keySet();
    }

    /**
     * Remove (begin, end)
     * @param begin
     * @param end
     */
    public void removeEdgeOneWay(NodeKey begin, NodeKey end) {
        HashMap<NodeKey, EdgeData> endToEdge = edges.get(begin);

        if (endToEdge != null) {
            endToEdge.remove(end);

            // delete empty entry
            if (endToEdge.isEmpty()) {
                edges.remove(begin);
            }
        }
    }

    /**
     * Remove (begin, end) and (end, begin)
     * @param begin
     * @param end
     */
    public void removeEdgeBi(NodeKey begin, NodeKey end) {
        removeEdgeOneWay(begin, end);
        removeEdgeOneWay(end, begin);
    }

    public Set<NodeKey> getNeighbors(NodeKey k) {
        HashMap<NodeKey,EdgeData> endToEdge = edges.get(k);

        if (endToEdge != null) {
            return endToEdge.keySet();
        }
        return null;
    }

    public boolean hasEdge(NodeKey k, NodeKey anotherKey) {
        return hasEdgeOneWay(k, anotherKey) || hasEdgeOneWay(anotherKey, k);
    }

    @Override
    public String toString() {
        StringBuilder display = new StringBuilder("\nCheck neighbors of each node ...\n");

        for (NodeKey currentNode : nodes.keySet()) {
            display.append("Neighbors of ").append(currentNode).append(":\n");

            Set<NodeKey> neighborsSet = getNeighbors(currentNode);
            if (neighborsSet != null) {
                for (NodeKey neighbor : neighborsSet) {
                    display.append(neighbor).append("\n");
                }
            } else {
                display.append("No neighbors.\n");
            }

            display.append('\n');
        }
        return display.toString();
    }

    public HashMap<NodeKey, NodeKey> breadthDo(NodeKey source, Function<NodeKey, Void> action) {
        Queue<NodeKey> fringe = new LinkedList<>();
        HashMap<NodeKey, NodeKey> visited = new HashMap<>();


        if (!hasVertex(source))
            return null;

        fringe.add(source);
        visited.put(source, source);

        while (!fringe.isEmpty()) {
            NodeKey currentNode = fringe.poll();

            // do
            action.apply(currentNode);

            // add neighbors in queue (iff !visited)
            Set<NodeKey> neighbors = getNeighbors(currentNode);
            if (neighbors != null) {
                for (NodeKey neighbor : neighbors) {
                    if (!visited.containsKey(neighbor)) {
                        visited.put(neighbor, currentNode);
                        fringe.add(neighbor);
                    }
                }
            }
        }
        return visited;
    }

    private class NodeItr {
        boolean analyzing;
        Iterator<NodeKey> neighborsItr;

        NodeItr() {
            analyzing = false;
            neighborsItr = null;
        }

        NodeItr(Iterator itr) {
            this.analyzing = false;
            this.neighborsItr = itr;
        }

        void setItr(Iterator<NodeKey> itr) {
            neighborsItr = itr;
        }

        boolean hasNext() {
            if (neighborsItr == null) return false;
            return neighborsItr.hasNext();
        }

        NodeKey next() {
            if (hasNext())
                return neighborsItr.next();
            return null;
        }

        boolean isAnalyzing() {
            return analyzing;
        }
        void setAnalyzing() {
            analyzing = true;
        }
    }

    // TODO with breadthDo only with BiConsumer
    public void forEachDF(NodeKey source, Consumer<NodeKey> initial, Consumer<NodeKey> middle, Consumer<Void> after) {
        Stack<NodeKey> fringe = new Stack<>();
        HashMap<NodeKey, NodeItr> visited = new HashMap<>();

        stepHelper(source, visited, fringe);
        initial.accept(source);

        while (!fringe.isEmpty()) {
            NodeKey currentNode = fringe.peek();
            NodeItr currentMeta = visited.get(currentNode);

            if (currentMeta.hasNext()) {
                NodeKey neighbor = currentMeta.next();
                if (neighbor == null) throw new AssertionError("[Debug] hasNext() was called! *** next() should not return null.");

                // attempt visiting neighbor
                if (!visited.containsKey(neighbor)) {
                    if (currentMeta.isAnalyzing()) { // if recurs again
                        initial.accept(currentNode);
                    } else {
                        currentMeta.setAnalyzing();
                    }

                    stepHelper(neighbor, visited, fringe); //meh
                    middle.accept(neighbor);
                }
            } else { // analyzed
                after.accept(null);
                fringe.pop();
            }
        }
    }

    private void stepHelper(NodeKey key,
                            HashMap<NodeKey, NodeItr> visited, Stack<NodeKey> fringe
    ) {
        Set<NodeKey> neighbors = getNeighbors(key);
        Iterator<NodeKey> neighborsItr = null;
        // Init neighborsItr
        if (neighbors != null) {
            neighborsItr = neighbors.iterator();
        }
        visited.put(key, new NodeItr(neighborsItr));
        fringe.add(key);
    }

    public NodeKey firstNode() {
        return nodes.keySet().iterator().next();
    }

    public void forEachNode(BiConsumer<NodeKey, NodeData> action) {
        nodes.forEach(action);
    }

    public enum CiclesPredicate {
        Allowed,
        None
    }

    private class NodeMeta {
        int t1, t2;
        NodeKey visitor;
        Iterator<NodeKey> neighborsItr;

        static final int unset = -1;

        NodeMeta() {
            visitor = null;
            t1 = unset;
            t2 = unset;
            neighborsItr = null;
        }

        NodeMeta(NodeKey key, int t1, Iterator itr) {
            if (key == null) throw new AssertionError("Visitor cannot be null!");
            visitor = key;
            this.t1 = t1;
            this.t2 = unset;
            this.neighborsItr = itr;
        }

        void set1(int i) {
            if (i == unset) throw new AssertionError("Cannot set to `unset'!");
            t1 = i;
        }

        void setItr(Iterator<NodeKey> itr) {
            neighborsItr = itr;
        }

        boolean hasNext() {
            if (neighborsItr == null) return false;
            return neighborsItr.hasNext();
        }

        NodeKey next() {
            if (hasNext())
                return neighborsItr.next();
            return null;
        }

        void set2(int i) {
            if (i == unset) throw new AssertionError("Cannot set to `unset'!");
            t2 = i;
        }

        void setVisitor(NodeKey key) { // keys are unique
            visitor = key;
        }

        int get1() {
            if (t1 == unset) throw new AssertionError("Cannot get `unset'!");
            return t1;
        }

        int get2() {
            if (t2 == unset) throw new AssertionError("Cannot get `unset'!");
            return t2;
        }

        NodeKey getVisitor(NodeKey key) {
            if (visitor == null) throw new AssertionError("Cannot get null visitor!");
            return visitor;
        }

        boolean analyzed() {
            return visited() && t2 != unset;
        }

        boolean visited() {
            return visitor != null && t1 != unset;
        }

        boolean onlyVisited() {
            return visited() && !analyzed();
        }
    }

    // MAYBE:
    // private class TotalDepthDoState {
    //         public NodeKey key, parent;
    //         public int t1;
    //         public HashMap<NodeKey, NodeMeta> visited;
    //         public HashSet<NodeKey> untouched;
    //         public Stack<NodeKey> fringe;

    //         TotalDepthDoState(NodeKey key, NodeKey parent, int t1,
    //                           HashMap<NodeKey, NodeMeta> visited, HashSet<NodeKey> untouched,
    //                           Stack<NodeKey> fringe) {
    //                 this.key = key;
    //                 this.t1 = t1;
    //                 this.visited = visited;
    //                 this.untouched = untouched;
    //                 this.fringe = fringe;
    //         }
    // }

    private void sflag(NodeKey key, NodeKey parent, int t1,
                       HashMap<NodeKey, NodeMeta> visited, HashSet<NodeKey> untouched,
                       Stack<NodeKey> fringe
    ) {
        Set<NodeKey> neighbors = getNeighbors(key);
        Iterator<NodeKey> neighborsItr = null;
        // Init neighborsItr
        if (neighbors != null) {
            neighborsItr = neighbors.iterator();
        }
        visited.put(key, new NodeMeta(parent, t1, neighborsItr));
        untouched.remove(key);
        fringe.add(key);
    }

    public List<NodeKey> topologicSort(NodeKey source) {
        List<NodeKey> res = new ArrayList<>();
        totalDepthDo(source,
                key -> null,
                key -> { res.add(key); return null; },
                Graph.CiclesPredicate.None);
        return res;
    }

    public HashMap<NodeKey, NodeMeta> totalDepthDo(NodeKey source,
                                                   Function<NodeKey, Void> visitedDo,
                                                   Function<NodeKey, Void> analyzedDo,
                                                   CiclesPredicate ciclesAllowed
    ) {
        if (!hasVertex(source))
            return null;

        Stack<NodeKey> fringe = new Stack<>();
        HashMap<NodeKey, NodeMeta> visited = new HashMap<>();
        HashSet<NodeKey> untouched = new HashSet<>(nodes.keySet());
        int t = 0;

        sflag(source, source, t++, visited, untouched, fringe); //meh
        visitedDo.apply(source);

        while(!untouched.isEmpty()) {
            while (!fringe.isEmpty()) {
                NodeKey currentNode = fringe.peek();

                NodeMeta currentMeta = visited.get(currentNode);
                if (currentMeta.hasNext()) {
                    NodeKey neighbor = currentMeta.next();
                    if (neighbor == null) throw new AssertionError("[Debug] hasNext() was called! *** next() should not return null.");

                    // attempt visting neighbor
                    if (!visited.containsKey(neighbor)) {
                        sflag(neighbor, currentNode, t++, visited, untouched, fringe);
                        visitedDo.apply(neighbor);
                    } else if (ciclesAllowed == CiclesPredicate.None &&
                            visited.get(neighbor).onlyVisited()) { // meaning currentNode is still in the neighbor's subtree
                        throw new AssertionError("A cicle was found! ciclesAllowed == None.");
                    }
                } else { // analyzed
                    analyzedDo.apply(currentNode);
                    visited.get(currentNode).set2(t++);
                    fringe.pop();
                }
            }

            // If all nodes have been visited
            if (untouched.isEmpty()) {
                return visited;
            }

            // start over
            source = untouched.iterator().next();
            sflag(source, source, t++, visited, untouched, fringe);
            visitedDo.apply(source);

            if (untouched.isEmpty()) {
                NodeKey currentNode = fringe.pop();
                analyzedDo.apply(currentNode);
                visited.get(currentNode).set2(t++);
            }
        }

        return visited;
    }

    public void treeDo(Consumer<NodeKey> initial, Consumer<NodeKey> recurr) {
        HashMap<NodeKey, NodeMeta> visited =
                totalDepthDo(firstNode(),
                        // (ign) -> { System.out.print(ign + " "); return null; },
                        (ign) -> null,
                        // (ign) -> { System.out.print(ign + " "); return null; },
                        (ign) -> null,
                        CiclesPredicate.None);
        final Stack<NodeKey> t2 = getSortedDescKeys(visited);

        while(!t2.isEmpty()) {
            System.out.println("Tree:");
            forEachDF(t2.peek(),
                    initial,
                    recurr,
                    (ign) -> t2.pop()
            );
            System.out.println("\n---");
        }
    }

    public void treeDoBi(Consumer<NodeKey> initial, Consumer<NodeKey> recurr) {
        HashMap<NodeKey, NodeMeta> visited =
                totalDepthDo(firstNode(),
                        // (ign) -> { System.out.print(ign + " "); return null; },
                        (ign) -> null,
                        // (ign) -> { System.out.print(ign + " "); return null; },
                        (ign) -> null,
                        CiclesPredicate.Allowed);
        final Stack<NodeKey> t2 = getSortedDescKeys(visited);

        while(!t2.isEmpty()) {
//                        System.out.println("Tree:");
            forEachDF(t2.peek(),
                    initial,
                    recurr,
                    (ign) -> t2.pop()
            );
            System.out.println("\n---");
        }
    }

    public Stack<NodeKey> getSortedDescKeys(HashMap<NodeKey, NodeMeta> visited) {
        final int size = 2 * visited.size();
        final Vector<NodeKey> t2 = new Vector<>(size);
        t2.setSize(size);

        visited.forEach(
                (key, meta) -> t2.set(meta.get2(), key));


        Stack<NodeKey> ret = new Stack<>();
        for (int i = 0; i < t2.size(); ++i) {
            if (t2.elementAt(i) != null) {
                ret.add(t2.elementAt(i));
            }
        }
        return ret;
    }

    public Vector<Vector<NodeKey>> getPathsFromVisited(HashMap<NodeKey, NodeKey> visited, NodeData endMarker) {

        Vector<Vector<NodeKey>> path = new Vector<>();

        visited.forEach((k, v) -> {
            if (getNodeData(k) == endMarker) {
                path.add(traceBackFrom(visited, k));
            }
        });

        return path;
    }

    public Vector<NodeKey> traceBackFrom(HashMap<NodeKey, NodeKey> visited, NodeKey current) {
        Vector<NodeKey> path = new Vector<>(1);

        path.add(0, current);

        while (current != visited.get(current)) {
            path.add(0, visited.get(current));
            current = visited.get(current);
        }

        return path;
    }

    public PriorityQueue<Triplet<NodeKey, NodeKey, EdgeData>> getSortedEdges(EdgeData DEFAULT_VALUE) {
        if (this.cap == -1) {
            throw new AssertionError("Specify inital capacity");
        }
        // the output: sorted edges by cost
        PriorityQueue<Triplet<NodeKey, NodeKey, EdgeData>> sortedEdges = new PriorityQueue<>();

        // add only (begin, end), and not also (end, begin)
        HashSet<NodeKey> visited = new HashSet<>(cap + 1);

        // For each vertex: O(m). Insert into Heap: lg m. => O (m * lg m)
        for (Map.Entry<NodeKey, HashMap<NodeKey, EdgeData>> beginTo : edges.entrySet()) {
            // get begin
            NodeKey begin = beginTo.getKey();
            visited.add(begin);

            // get ends and costs
            HashMap<NodeKey, EdgeData> endsAndCosts = beginTo.getValue();
            if (null == endsAndCosts) {
                throw new AssertionError("Should not happen");
            }

            // for each ..., end]
            for (Map.Entry<NodeKey, EdgeData> endAndCost : endsAndCosts.entrySet()) {
                // get end
                NodeKey end = endAndCost.getKey();
                if (visited.contains(end)) {
                    continue;
                }

                // get cost
                EdgeData cost = endAndCost.getValue();

                // make a triplet (begin, end, edgeCost)
                Triplet<NodeKey, NodeKey, EdgeData> triplet;
                triplet = new Triplet<>(begin, end, cost);

                // put in priority queue; MinHeap of edgeCost
                sortedEdges.add(triplet);
            }
        }

        return sortedEdges;
    }



    /**
     * here
     * @param begin
     * @param end
     * @param nullCost
     * @param add
     * @return
     */
    public HashMap<NodeKey, NodeKey> Dijkstra(NodeKey begin, NodeKey end, EdgeData nullCost, BiFunction<EdgeData, EdgeData, EdgeData> add) {
        HashMap<NodeKey, NodeKey> visited = new HashMap<>();
        HashMap<NodeKey, EdgeData> cost = new HashMap<>();

        visited.put(begin, begin);
        cost.put(begin, nullCost);

        PriorityQueue<Pair<NodeKey, EdgeData>> fringe = new PriorityQueue<>();
        fringe.add(new Pair<>(begin, nullCost));

        while (!fringe.isEmpty()) {
            Pair<NodeKey, EdgeData> entry = fringe.poll();
            NodeKey node = entry.getFirst();

            // reached goal?
            if (end.equals(node)) {
                return visited;
            }

            for (NodeKey neighbor : getNeighbors(node)) {
                EdgeData soFarCost = add.apply(cost.get(node), getEdgeData(node, neighbor));

                // !visited(neightbor) || x + b(x, neighbor) < cost(neighbor)
                if (!visited.containsKey(neighbor) ||
                        soFarCost.compareTo(cost.get(neighbor)) < 0) {
                    visited.put(neighbor, node);
                    cost.put(neighbor, soFarCost);
                    fringe.add(new Pair<>(neighbor, soFarCost));
                }
            }
        }

        return null;
    }

    public HashMap<NodeKey, NodeKey> aStarSearch(NodeKey begin, NodeKey end,
                                                 EdgeData nullCost, BiFunction<EdgeData, EdgeData, EdgeData> add,
                                                 BiFunction<NodeData, NodeData, EdgeData> heuristic) {
        HashMap<NodeKey, NodeKey> visited = new HashMap<>();
        HashMap<NodeKey, EdgeData> cost = new HashMap<>();

        visited.put(begin, begin);
        cost.put(begin, nullCost);

        PriorityQueue<Pair<NodeKey, EdgeData>> fringe = new PriorityQueue<>();
        fringe.add(new Pair<>(begin, nullCost));

        while (!fringe.isEmpty()) {
            Pair<NodeKey, EdgeData> entry = fringe.poll();
            NodeKey node = entry.getFirst();

            // reached goal?
            if (end.equals(node)) {
                return visited;
            }

            for (NodeKey neighbor : getNeighbors(node)) {
                EdgeData soFarCost = add.apply(cost.get(node), getEdgeData(node, neighbor));

                // !visited(neighbor) || x + b(x, neighbor) < cost(neighbor)
                if (!visited.containsKey(neighbor) ||
                        soFarCost.compareTo(cost.get(neighbor)) < 0) {
                    visited.put(neighbor, node);
                    cost.put(neighbor, soFarCost);

                    // priority = soFarCost + heuristic(node, neighbor)
                    EdgeData priority = add.apply(soFarCost,
                            heuristic.apply(getNodeData(neighbor), getNodeData(end)));
                    fringe.add(new Pair<>(neighbor, priority));
                }
            }
        }

        return null;
    }

    public List<NodeKey> getPathFromVisited(HashMap<NodeKey, NodeKey> visited, NodeKey end) {
        List<NodeKey> path = new ArrayList<>();

        path.add(end);
        NodeKey current = end;

        while (visited.get(current) != current) {
            path.add(0, visited.get(current));
            current = visited.get(current);
        }
        return path;
    }

    public Pair<HashMap<NodeKey, NodeKey>, Boolean> bellmanford(NodeKey source, NodeKey stock,
                                                                EdgeData nullCost,
                                                                BiFunction<EdgeData, EdgeData, EdgeData> addEdgeData) {
        final HashMap<NodeKey, NodeKey> visited = new HashMap<>();
        final HashMap<NodeKey, EdgeData> dist = new HashMap<>();

        visited.put(source, source);
        dist.put(source, nullCost);

        for (int i = 0; i < getNumVertices() - 1; ++i) {
            forEachEdge((begin, end, edgeData) -> {
                // dist[v] = inf
                if (!visited.containsKey(begin)) return;

                if (!visited.containsKey(end) ||
                        // dist[begin] + edgeCost < dist[end]
                        addEdgeData.apply(dist.get(begin), edgeData).compareTo(dist.get(end)) < 0) {
                    visited.put(end, begin);
                    dist.put(end, addEdgeData.apply(dist.get(begin), edgeData));
                }
            });
        }

        BoolBox negativeCycles = new BoolBox(false);
        forEachEdge((begin, end, edgeData) -> {
            if (negativeCycles.get()) return;
            if (visited.containsKey(begin) && !visited.containsKey(end)) {
                return;
            } else if (!visited.containsKey(begin)) {
                return;
            }

            // dist[begin] + edgeCost < dist[end]
            if (addEdgeData.apply(dist.get(begin), edgeData).compareTo(dist.get(end)) < 0) {
                negativeCycles.set(true);
            }
        });

                /*
                for i=0,n-1
                        for each (u, v) in edges:
                                if (!visited[u]) continue;
                                if (!visited[v] || dist[u] + cost(u, v) < dist[v]):
                                        // update dist[v] and visited[v]
                 */
        return new Pair<>(visited, negativeCycles.get());
    }

    public static <NodeKey> List<NodeKey> getCycleFromVisited(HashMap<NodeKey, NodeKey> visited, NodeKey begin) {
        List<NodeKey> path = new ArrayList<>();
        HashSet<NodeKey> added = new HashSet<>();

        path.add(begin);
        added.add(begin);
        NodeKey current = begin;

        while (visited.get(current) != current) {
            path.add(0, visited.get(current));
            // if already added -> found cycle
            if (added.contains(visited.get(current))) {
                break;
            }
            added.add(path.get(0));
            current = visited.get(current);
        }

        // remove nodes up until cycle
        while (!path.get(path.size()-1).equals(path.get(0))) {
            path.remove(path.size()-1);
        }


        return path;
    }

    class BoolBox {
        boolean bool;

        BoolBox(boolean bool) {
            this.bool = bool;
        }

        void set(boolean bool) {
            this.bool = bool;
        }

        boolean get() {
            return this.bool;
        }
    }

    public void forEachEdge(TriConsumer<NodeKey, NodeKey, EdgeData> action) {
        for (Map.Entry<NodeKey, HashMap<NodeKey, EdgeData>> beginTo : getEdgesSet()) {
            NodeKey begin = beginTo.getKey();

            for (Map.Entry<NodeKey, EdgeData> endAndData : beginTo.getValue().entrySet()) {
                NodeKey end = endAndData.getKey();
                EdgeData edgeData = endAndData.getValue();

                action.accept(begin, end, edgeData);
            }
        }
    }

    public Set<Map.Entry<NodeKey, HashMap<NodeKey, EdgeData>>> getEdgesSet() {
        return edges.entrySet();
    }
    public Set<Map.Entry<NodeKey, NodeData>> getNodes() {
        return nodes.entrySet();
    }
}
