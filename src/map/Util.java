package map;

import containers.Pair;
import java.lang.Math;

public abstract class Util {
    public static double euclideanDistance(Pair<Integer, Integer> lhs, Pair<Integer, Integer> rhs) {
        double xLen = Math.abs(lhs.getFirst() - rhs.getFirst());
        double yLen = Math.abs(lhs.getSecond() - rhs.getSecond());

        // sqrt(xLen^2, yLen^2)
        return Math.sqrt(Math.pow(xLen, 2) + Math.pow(yLen, 2));
    }
}
