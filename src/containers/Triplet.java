package containers;

public class Triplet<T, U, V extends Comparable<V>> implements Comparable<Triplet<T, U, V>> {
    private final T first;
    private final U second;
    private final V third;

    public Triplet(T first, U second, V third) {
        if (null == first || null == second || null == third) {
            throw new AssertionError("No null data");
        }

        this.first = first;
        this.second = second;
        this.third = third;
    }

    public boolean same(Triplet<T, U, V> other) {
        if (null == other) return false;
        return this == other || first.equals(other.first) && second.equals(other.second) && third.equals(other.third);
    }

    @Override
    public String toString() {
        return "{" + first + ", " + second + ", " + third + '}';
    }

    @Override
    public int compareTo(Triplet<T, U, V> o) {
        return this.third.compareTo(o.third);
    }

    public T getFirst() {
        return first;
    }

    public U getSecond() {
        return second;
    }

    public V getThird() {
        return third;
    }
}
