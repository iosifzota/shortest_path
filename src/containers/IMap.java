package containers;

import java.util.Set;

public interface IMap<K, V> {
    boolean containsKey(K key);
    V get(K key);
    boolean isEmpty();
    Set<K> keySet();
    V put(K key, V value);
    V remove(K key);
    int size();
}
